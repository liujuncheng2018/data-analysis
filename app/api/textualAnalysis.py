
from flask import Blueprint,  request

import jieba
import jieba.analyse
import ast
import re
from collections import Counter

# 文本分析
# -----------------------------------------------------------------------------------------------
# 输入 文本、分词个数
# 输出 分词列表，个数
from snownlp import SnowNLP

textualAnalysis_api=Blueprint('textualAnalysis_api',__name__)

@textualAnalysis_api.route('/cut',methods=["POST"])
def cut_words():
    # 接收请求参数
    text = request.form.get('text')
    num_words = int(request.form.get('num_words'))

    # 分词
    seg_list = jieba.cut(text)
    # 过滤标点符号
    pattern = re.compile(r'[\u4e00-\u9fa5]+')
    words = [word for word in seg_list if pattern.match(word)]
    # 使用collections模块的Counter函数统计词频
    word_count = Counter(words)
    # 获取词频前topk的词和出现次数
    top_words = word_count.most_common(num_words)
    # 返回分词列表和词频
    return [(word, count) for word, count in top_words]


# 输入 文本，关键词个数
# 输出 关键词列表
@textualAnalysis_api.route('/getkeyWords',methods=["POST"])
def extract_keywords():
    text = request.form.get('text')
    topK  = int(request.form.get('topK'))
    # 使用 jieba 库进行分词和关键词提取
    keywords = jieba.analyse.extract_tags(text, topK=topK)
    return keywords


# 输入 短语或者问题
# 输出 有关文本
@textualAnalysis_api.route('/answer',methods=["POST"])
def extract_articles():
    dataset=request.form.get("dataset")
    # str转list
    dataset = ast.literal_eval(dataset)
    keyword=request.form.get("keyword")
    """
    从文本数据集中提取包含关键词的文章

    参数：
    dataset：包含多篇文章的文本数据集，类型为列表，每个元素为一个字符串，代表一篇文章。
    keyword：关键词，类型为字符串，用于指定需要提取的文章。

    返回值：
    包含关键词的文章列表，类型为列表，每个元素为一个字符串，代表一篇文章。
    """
    articles = []
    for article in dataset:
        if keyword in article:

            articles.append(article)

    return {"articles":articles}

@textualAnalysis_api.route('/predict/emotion',methods=["POST"])
def emoPrediction():
    # 接收前端转来文本
    text = request.form.get("text")

    # 初始化SnowNLP对象
    s = SnowNLP(text)

    # 获取情感倾向，返回值为一个介于0和1之间的浮点数，越接近1表示情感越积极，越接近0表示情感越消极
    sentiment = s.sentiments

    # 输出情感倾向
    if sentiment > 0.6:
         return {"emotion":"正面"}
    elif sentiment < 0.5:
         return {"emotion":"负面"}
    else:
         return {"emotion":"中立"}

# text = "该同学为人随和，易于沟通，热情大方，生活充实，有严谨的生活态度。学习上态度端正，目标明确，不断提高自身的思想文化素质。"
# "我毕业来国外连续干了快2年了，去年11月我活干的很糟心，焦虑失眠，没食欲，下班游戏都不想打了。现在我还没回去。"