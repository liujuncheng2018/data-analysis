from json_flask import JsonFlask
from json_response import JsonResponse
from api.textualAnalysis import textualAnalysis_api
from api.descriptiveStatistics import descriptiveStatistics_api
from api.inferentialStatistics import inferentialStatistics_api
from api.multivariateStatistics import multivariateStatistics_api
from api.bigData import bigData_api
app = JsonFlask(__name__)
# 初始化生成一个app对象，这个对象就是Flask的当前实例对象，后面的各个方法调用都是这个实例
# Flask会进行一系列自己的初始化，比如web API路径初始化，web资源加载，日志模块创建等。然后返回这个创建好的对象给你

# 注册蓝图
app.register_blueprint(textualAnalysis_api,url_prefix='/nan/nlp')
app.register_blueprint(multivariateStatistics_api,url_prefix='/nan/stru/test/mvar')
app.register_blueprint(inferentialStatistics_api,url_prefix='/nan/stru')
app.register_blueprint(descriptiveStatistics_api,url_prefix='/ana/stru/measnum')
app.register_blueprint(bigData_api,url_prefix='/nan/stru/')

@app.route("/")    # 自定义路径
def index():
    return 'Hello!'

@app.errorhandler(Exception)
def error_handler(e):
    """
    全局异常捕获，也相当于一个视图函数
    """
    return JsonResponse.error(msg=str(e))

if __name__ == '__main__':
    app.run(debug=True, host='127.0.0.1', port=8888)   #
# flask默认是没有开启debug模式的，开启debug模式，可以帮助我们查找代码里面的错误
# host = '127.0.0.1' 表示设置的ip，如果需要连接手机等设备，可以将手机和电脑连接同一个热点，将host设置成对应的ip
# port 为端口号，可自行设置
